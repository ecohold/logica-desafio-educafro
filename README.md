# Projeto Desafio Final Educafro - Lógica de Programação - Aula 12

Descrição:

Este projeto implementa dois sistemas distintos a partir dos requisitos e regras de negócio.

1. Nubank - Operações bancárias

- Funcionalidades:
Simulação de saques e depósitos em conta corrente.
Verificação de saldo disponível e mínimo (R$ 100,00).
Exibição do saldo atualizado após cada transação.
- Tecnologias:
Banco de dados: PostgreSQL com MVCC e multitenancy
APIs: 2 APIs RESTful (CRUD)
Balanceador de carga: Nginx
Banco de dados: SQLite com MVCC e multitenancy
Concorrência: Gerenciada pelo sistema operacional
Docker: Dockerfile, imagem Docker e Docker Compose
- Limitações: 1,5 CPU e 500 MB de memória para todo o sistema
- Testes:
Testes unitários, de integração, funcionais, de profiling e de stress para garantir a qualidade e robustez do sistema.

2. Restaurante Amadeus:

Funcionalidades:
Menu com 3 opções de pratos e seus preços por pessoa.
Cálculo do valor total para duas pessoas.
Exibição do preço final do prato escolhido.
Tecnologias:
Banco de dados: PostgreSQL com MVCC e multitenancy
APIs: 2 APIs RESTful (CRUD)
Balanceador de carga: Nginx
Ferramenta de teste: Curl
Docker: Dockerfile, imagem Docker e Docker Compose
Limitações: 1,5 CPU e 500 MB de memória para todo o sistema
Testes:
Testes unitários, de integração, funcionais, de profiling e de stress para garantir a qualidade e robustez do sistema.
Observações:

3. O sistema bancário utiliza uma implementação própria do Nubank in-memory enquanto o restaurante Amadeus utiliza o PostgreSQL.
Ambos os sistemas possuem 2 APIs RESTful (CRUD) e utilizam o Nginx como balanceador de carga.
O Docker é utilizado para containerizar e gerenciar os sistemas, com limites de 1,5 CPU e 500 MB de memória para todo o sistema.
Bateria completa de testes garante a qualidade e robustez dos sistemas.
Requisitos:

4. Implementar todas as funcionalidades descritas para ambos os sistemas.
Utilizar as tecnologias especificadas para cada sistema.
Realizar testes unitários, de integração, funcionais, de profiling e de stress.
Limitar o uso de recursos a 1,5 CPU e 500 MB de memória para todo o sistema.
Recursos Adicionais:

Documentação do Nubank: https://nubank.com.br/
Documentação do PostgreSQL: https://www.postgresql.org/docs/current/
Documentação do Nginx: https://nginx.org/en/docs/
Documentação do Docker: https://docs.docker.com/

5. Dicas:

Comece definindo a arquitetura de cada sistema.
Utilize frameworks para desenvolvimento web (Django, Flask) ou faça na unha
Utilize bibliotecas para testes (pytest, unittest).
Utilize ferramentas de profiling (cProfile, gprof).
Utilize ferramentas de stress (Apache JMeter, Gatling).
Este projeto é um desafio de lógica de programação e visa testar suas habilidades em:

Implementação de algoritmos.
Arquitetura de software.
Uso de tecnologias de ponta.
Realização de testes.
Otimização de desempenho.

# Arquitetura de Software:
Implementação de duas APIs RESTful com CRUD (Create, Read, Update, Delete).
Balanceamento de carga com Nginx.
Banco de dados SQLite com MVCC (Multiversion Concurrency Control) e multitenancy.
Dockerfiles para containerização das aplicações.
Docker Compose para gerenciamento do ambiente de desenvolvimento.
Testes:
Testes unitários para cada funcionalidade.
Testes de integração para garantir a comunicação entre os componentes.
Testes funcionais para verificar o comportamento geral do sistema.
Testes de profiling para identificar gargalos de desempenho.
Testes de stress para avaliar a capacidade do sistema sob carga.
Requisitos:

Simulação Bancária:

Saldo inicial: R$ 1.000,00.
Saldo mínimo: R$ 100,00.
Saque e depósito de valores.
Verificação de saldo disponível e mínimo.
Exibição do saldo atualizado após cada transação.
Menu de Restaurante:

Restaurante Amadeus com 3 opções de pratos:
Lagosta ao molho rosé: R$ 120,00 por pessoa.
Moqueca de camarão: R$ 100,00 por pessoa.
Filé de peixe com arroz branco: R$ 50,00 por pessoa.
Cálculo do valor total para duas pessoas.
Exibição do preço final do prato escolhido.
Recursos:

Banco: App Nubank (opcional)
Balanceador de Carga: Nginx
Banco de Dados: SQLite
Linguagem de Programação: Python (recomendado)
Ferramentas de Teste:
Unittest (Python)
Postman (API)
JMeter (carga)
Docker:
Dockerfile para cada aplicação
Docker Compose para gerenciamento do ambiente
Etapas:

Planejamento:

Definir a arquitetura do sistema.
Criar diagramas de classes e de sequência.
Especificar os requisitos de cada API.
Desenvolvimento:

Implementar as APIs com CRUD.
Implementar o balanceador de carga com Nginx.
Configurar o banco de dados SQLite com MVCC e multitenancy.
Criar Dockerfiles para cada aplicação.
Escrever testes unitários, de integração, funcionais, de profiling e de stress.
Testes:

Executar os testes unitários e de integração.
Testar as APIs com Postman.
Realizar testes funcionais e de carga com JMeter.
Ajustar o código e otimizar o desempenho.
Deploy:

Criar imagens Docker para as aplicações.
Usar o Docker Compose para subir o ambiente de produção.
Monitorar o sistema e realizar testes de penetração (opcional).
Dicas:

Utilize ferramentas de versionamento de código (Git).
Siga boas práticas de codificação e documentação.
Comece com funcionalidades básicas e implemente as mais complexas gradualmente.
Teste cada etapa antes de passar para a próxima.
Utilize a comunidade online para obter ajuda e solucionar dúvidas.
Recursos Adicionais:

Documentação do SQLite: https://www.sqlite.org/docs.html
Tutoriais sobre Nginx: [URL inválido removido]
Tutoriais sobre Docker: https://docs.docker.com/get-started/
Tutoriais sobre testes de software: [URL inválido removido]
Observações:

Esta implementação é apenas um guia e pode ser adaptada de acordo com suas necessidades e conhecimentos.
A escolha da linguagem de programação, ferramentas e bibliotecas é livre.
O foco principal deve estar na implementação correta das regras de negócio e na aplicação dos conceitos de lógica de programação.
Conclusão:

# Requisitos:

Arquitetura de Arquivos e Pastas para os Apps Nubank e Amadeus Restaurante
Considerações Iniciais:

Ambas as arquiteturas são simples, poderosas, completas e seguem o padrão GCP Senior DevOp.
As estruturas foram validadas, testadas, documentadas e auditadas.
Arquitetura de Pastas para o App Nubank:

      App Nubank:
      app/
      ├── api/
      │ ├── controllers.py
      │ ├── models.py
      │ ├── routes.py
      │ └── views.py
      ├── config.py
      ├── create_database.py
      ├── database/
      │ ├── migrations/
      │ └── schema.sql
      ├── main.py
      ├── public/
      ├── static/
      ├── tests/
      └── utils.py
7. Recursos Adicionais:
Documentação do SQLite: https://www.sqlite.org/docs.html
Tutoriais sobre Python: https://dle.rae.es/inv%C3%A1lido
8. Próximos Passos:
Com a criação do banco de dados automatizada, você pode seguir para o desenvolvimento das APIs e da interface do app.

#----------------------agordagem 2 ------------------------------#

Criação Automatizada do Banco de Dados no App Nubank
Objetivo:
Assegurar que a criação do banco de dados SQLite do App Nubank aconteça automaticamente via app, com documentação completa e versionamento.
Atualização da Estrutura do Projeto:
1. Adição de Novos Arquivos:
app/database/init.py
app/database/alembic.ini
2. Modificação do Arquivo schema.sql:
Mover as instruções SQL para um arquivo separado app/database/migrations/base.sql.
3. Estrutura Atualizada:

    app/
    ├── api/
    │ ├── controllers.py
    │ ├── models.py
    │ ├── routes.py
    │ └── views.py
    ├── config.py
    ├── database/
    │ ├── alembic.ini
    │ ├── migrations/
    │ │ └── base.sql
    │ └── init.py
    ├── main.py
    ├── public/
    ├── static/
    ├── tests/
    └── utils.py


7. Recursos Adicionais:
Documentação do SQLite: https://www.sqlite.org/docs.html
Tutoriais sobre Python: https://dle.rae.es/inv%C3%A1lido
8. Próximos Passos:
Com a criação do banco de dados automatizada, você pode seguir para o desenvolvimento das APIs e da interface do app.

#----------------------agordagem 2 ------------------------------#

Criação Automatizada do Banco de Dados no App Nubank
Objetivo:
Assegurar que a criação do banco de dados SQLite do App Nubank aconteça automaticamente via app, com documentação completa e versionamento.
Atualização da Estrutura do Projeto:
1. Adição de Novos Arquivos:
app/database/init.py
app/database/alembic.ini
2. Modificação do Arquivo schema.sql:
Mover as instruções SQL para um arquivo separado app/database/migrations/base.sql.
3. Estrutura Atualizada:

      app/
      ├── api/
      │ ├── controllers.py
      │ ├── models.py
      │ ├── routes.py
      │ └── views.py
      ├── config.py
      ├── create_database.py
      ├── database/
      │ ├── migrations/
      │ └── schema.sql
      ├── main.py
      ├── public/
      ├── static/
      ├── tests/
      └── utils.py
7. Recursos Adicionais:
Documentação do SQLite: https://www.sqlite.org/docs.html
Tutoriais sobre Python: https://dle.rae.es/inv%C3%A1lido
8. Próximos Passos:
Com a criação do banco de dados automatizada, você pode seguir para o desenvolvimento das APIs e da interface do app.

#----------------------agordagem 2 ------------------------------#

Criação Automatizada do Banco de Dados no App Nubank
Objetivo:
Assegurar que a criação do banco de dados SQLite do App Nubank aconteça automaticamente via app, com documentação completa e versionamento.
Atualização da Estrutura do Projeto:
1. Adição de Novos Arquivos:
app/database/init.py
app/database/alembic.ini
2. Modificação do Arquivo schema.sql:
Mover as instruções SQL para um arquivo separado app/database/migrations/base.sql.
3. Estrutura Atualizada:

    app/
    ├── api/
    │ ├── controllers.py
    │ ├── models.py
    │ ├── routes.py
    │ └── views.py
    ├── config.py
    ├── database/
    │ ├── alembic.ini
    │ ├── migrations/
    │ │ └── base.sql
    │ └── init.py
    ├── main.py
    ├── public/
    ├── static/
    ├── tests/
    └── utils.py

7. Recursos Adicionais:
Documentação do SQLite: https://www.sqlite.org/docs.html
Tutoriais sobre Python: https://dle.rae.es/inv%C3%A1lido
8. Próximos Passos:
Com a criação do banco de dados automatizada, você pode seguir para o desenvolvimento das APIs e da interface do app.

#----------------------agordagem 2 ------------------------------#

Criação Automatizada do Banco de Dados no App Nubank
Objetivo:
Assegurar que a criação do banco de dados SQLite do App Nubank aconteça automaticamente via app, com documentação completa e versionamento.
Atualização da Estrutura do Projeto:
1. Adição de Novos Arquivos:
app/database/init.py
app/database/alembic.ini
2. Modificação do Arquivo schema.sql:
Mover as instruções SQL para um arquivo separado app/database/migrations/base.sql.
3. Estrutura Atualizada:

    app/
    ├── api/
    │ ├── controllers.py
    │ ├── models.py
    │ ├── routes.py
    │ └── views.py
    ├── config.py
    ├── database/
    │ ├── alembic.ini
    │ ├── migrations/
    │ │ └── base.sql
    │ └── init.py
    ├── main.py
    ├── public/
    ├── static/
    ├── tests/
    └── utils.py

7. Recursos Adicionais:
Documentação do SQLite: https://www.sqlite.org/docs.html
Tutoriais sobre Python: https://dle.rae.es/inv%C3%A1lido
8. Próximos Passos:
Com a criação do banco de dados automatizada, você pode seguir para o desenvolvimento das APIs e da interface do app.

#----------------------agordagem 2 ------------------------------#

Criação Automatizada do Banco de Dados no App Nubank
Objetivo:
Assegurar que a criação do banco de dados SQLite do App Nubank aconteça automaticamente via app, com documentação completa e versionamento.
Atualização da Estrutura do Projeto:
1. Adição de Novos Arquivos:
app/database/init.py
app/database/alembic.ini
2. Modificação do Arquivo schema.sql:
Mover as instruções SQL para um arquivo separado app/database/migrations/base.sql.
3. Estrutura Atualizada:

        app/
        ├── api/
        │ ├── controllers.py
        │ ├── models.py
        │ ├── routes.py
        │ └── views.py
        ├── config.py
        ├── database/
        │ ├── alembic.ini
        │ ├── migrations/
        │ │ └── base.sql
        │ └── init.py
        ├── main.py
        ├── public/
        ├── static/
        ├── tests/
        └── utils.py

Arquitetura de Pastas para App Nubank (Senior DevOp)
app/api/controllers/

conta_controller.py: Controla as operações da conta.
transacao_controller.py: Controla as transações.
app/api/models/

conta.py: Modelo da conta.
transacao.py: Modelo da transação.
app/api/routes/

conta_routes.py: Rotas da conta.
transacao_routes.py: Rotas da transação.
app/api/views/

index.html: Página inicial da API (opcional).
app/config/

app.config: Configurações da aplicação.
database.config: Configurações do banco de dados.
app/database/

migrations: Migrações do banco de dados.
sqlite.db: Banco de dados SQLite.
app/public/

favicon.ico: Favicon da aplicação (opcional).
app/static/

css: Arquivos CSS.
js: Arquivos JavaScript.
imagens: Imagens da aplicação.
app/tests/

test_conta.py: Testes da conta.
test_transacao.py: Testes da transação.
app/utils/

utils.py: Funções utilitárias.
Dockerfile

Define como construir a imagem Docker da aplicação.
README.md

Documentação da aplicação.


# Arquitetura de Pastas para o App Amadeus Restaurante:

App Amadeus Restaurante:

        app/
      ├── api/
      │ ├── controllers.py
      │ ├── models.py
      │ ├── routes.py
      │ └── views.py
      ├── config.py
      ├── create_database.py
      ├── database/
      │ ├── migrations/
      │ └── schema.sql
      ├── main.py
      ├── public/
      ├── static/
      ├── tests/
      └── utils.py

Arquitetura de Pastas para App Amadeus Restaurante (Senior DevOp)
app/api/controllers/

menu_controller.py: Controla o menu do restaurante.
pedido_controller.py: Controla os pedidos.
app/api/models/

item_menu.py: Modelo do item do menu.
pedido.py: Modelo do pedido.
app/api/routes/

menu_routes.py: Rotas do menu.
pedido_routes.py: Rotas do pedido.
app/api/views/

index.html: Página inicial da API (opcional).
app/config/

app.config: Configurações da aplicação.
database.config: Configurações do banco de dados.
app/database/

migrations: Migrações do banco de dados.
sqlite.db: Banco de dados SQLite.
app/menu.json

Menu do restaurante em formato JSON.
app/public/

favicon.ico: Favicon da aplicação (opcional).
app/static/

css: Arquivos CSS.
js: Arquivos JavaScript.
imagens: Imagens da aplicação.
app/tests/

test_menu.py: Testes do menu.
test_pedido.py: Testes do pedido.
app/utils/

utils.py: Funções utilitárias.
Dockerfile

Define como construir a imagem Docker da aplicação.
README.md

Documentação da aplicação.

Dicas:

Comecei definindo a arquitetura do projeto.
Utilizei testes unitários para garantir a qualidade do código.
Faça backups frequentes.
Documentei o código e as APIs.

Este é um desafio complexo, mas gratificante.
Utilizei os recursos e ferramentas disponíveis no ubuntu 22 e na web como gcp e azure free para me auxiliar.
Busquei ajuda e orientação necessário na rinhadebackend na impossibilidade de acessar o wattapp do grupo e troquei mensagens por @ com a professora.

Acreditei em meu potencial e finalizei este projeto com sucesso!

Este Desafio Final é uma oportunidade bem interessante para estimular e desenvolver um projeto completo e profissional, demonstrando suas habilidades e conhecimentos em diversas áreas da programação.

Acontece num momento e aproveito que estou fritando o côco na #rinhadebackend-2024-Q1