from alembic import command
from alembic.config import Config

def create_database():
  """Cria o banco de dados SQLite."""

  # Obter o caminho do arquivo alembic.ini
  alembic_ini = os.path.join("database", "alembic.ini")

  # Criar a configuração do Alembic
  config = Config(alembic_ini)

  # Executar o comando upgrade para criar o banco de dados
  command.upgrade(config, "head")

if __name__ == "__main__":
  create_database()
