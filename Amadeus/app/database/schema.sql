-- Cria a tabela ItemMenu
CREATE TABLE IF NOT EXISTS item_menu (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nome VARCHAR(255) NOT NULL,
  preco_por_pessoa DECIMAL(10,2) NOT NULL
);

-- Cria a tabela Pedido
CREATE TABLE IF NOT EXISTS pedido (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  item_menu_id INTEGER NOT NULL,
  numero_pessoas INTEGER NOT NULL,
  valor_total DECIMAL(10,2) NOT NULL
);

-- Insere dados iniciais na tabela ItemMenu
INSERT INTO item_menu (nome, preco_por_pessoa) VALUES ('Lagosta ao molho rosé', 120.00);
INSERT INTO item_menu (nome, preco_por_pessoa) VALUES ('Moqueca de camarão', 100.00);
INSERT INTO item_menu (nome, preco_por_pessoa) VALUES ('Filé de peixe com arroz branco', 50.00);
