import os
import sqlite3

def create_database():
  """Cria o banco de dados SQLite."""

  # Obter o caminho do arquivo schema.sql
  schema_file = os.path.join("database", "schema.sql")

  # Conectar ao banco de dados
  connection = sqlite3.connect("database.sqlite")
  cursor = connection.cursor()

  # Executar o script schema.sql
  with open(schema_file, "r") as f:
    cursor.executescript(f.read())

  # Fechar a conexão
  connection.close()

if __name__ == "__main__":
  create_database()
