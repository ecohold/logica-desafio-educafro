-- Cria a tabela Conta
CREATE TABLE IF NOT EXISTS conta (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  nome VARCHAR(255) NOT NULL,
  saldo DECIMAL(10,2) NOT NULL DEFAULT 1000.00,
  saldo_minimo DECIMAL(10,2) NOT NULL DEFAULT 100.00
);

-- Cria a tabela Transacao
CREATE TABLE IF NOT EXISTS transacao (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  conta_id INTEGER NOT NULL,
  tipo VARCHAR(10) NOT NULL,
  valor DECIMAL(10,2) NOT NULL,
  data_hora DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- Insere dados iniciais na tabela Conta
INSERT INTO conta (nome, saldo) VALUES ('Nubank', 1000.00);
